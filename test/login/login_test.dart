// ignore_for_file: non_constant_identifier_names

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tots_challenge/entities/user/user.dart';
import 'package:tots_challenge/features/login/bloc/login_bloc.dart';
import 'package:tots_challenge/repositories/user_repo.dart';

void main() {
  group(
    'Login bloc',
    () {
      late LoginBloc loginBloc;
      late LoginState loginState;

      late UserRepository userRepo;

      setUp(() {
        userRepo = MockUserRepository();
        loginBloc = LoginBloc(userRepo: userRepo);
        loginState = const LoginState();

        /// Initialize flutter binding.
        TestWidgetsFlutterBinding.ensureInitialized();

        /// Initialize shared preferences testing values.
        SharedPreferences.setMockInitialValues({});
      });

      group('On login submited', () {
        final user = MockUser();

        blocTest(
          'Login submited event - success',
          build: () => loginBloc,
          seed: () => loginState,
          act: (bloc) => bloc.add(
            const LoginSubmitted(
              email: 'email',
              password: 'password',
            ),
          ),
          setUp: () {
            when(
              () => userRepo.login(
                email: any(named: 'email'),
                password: any(named: 'password'),
              ),
            ).thenAnswer((_) async => (user, null));
          },
          expect: () => [
            loginState.copyWith(status: LoginStatus.loading),
            loginState.copyWith(
              status: LoginStatus.success,
              user: user,
            ),
          ],
        );

        blocTest(
          'Login submited event - failure on error',
          build: () => loginBloc,
          seed: () => loginState,
          act: (bloc) => bloc.add(
            const LoginSubmitted(
              email: 'email',
              password: 'password',
            ),
          ),
          setUp: () {
            when(
              () => userRepo.login(
                email: any(named: 'email'),
                password: any(named: 'password'),
              ),
            ).thenAnswer((_) async => (user, 'error'));
          },
          expect: () => [
            loginState.copyWith(status: LoginStatus.loading),
            loginState.copyWith(
              status: LoginStatus.failure,
              error: 'error',
            ),
          ],
        );

        blocTest(
          'Login submited event - failure on exception',
          build: () => loginBloc,
          seed: () => loginState,
          act: (bloc) => bloc.add(
            const LoginSubmitted(
              email: 'email',
              password: 'password',
            ),
          ),
          setUp: () {
            when(
              () => userRepo.login(
                email: any(named: 'email'),
                password: any(named: 'password'),
              ),
            ).thenThrow(Exception('error'));
          },
          expect: () => [
            loginState.copyWith(status: LoginStatus.loading),
            loginState.copyWith(
              status: LoginStatus.failure,
              error: 'Exception: error',
            ),
          ],
        );
      });
    },
  );
}

// Mocks
class MockUserRepository extends Mock implements UserRepository {}

class MockUser extends Mock implements User {
  @override
  String get access_token => 'token';
}
