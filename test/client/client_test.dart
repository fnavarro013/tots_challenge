// ignore_for_file: non_constant_identifier_names

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:tots_challenge/entities/client/client.dart';
import 'package:tots_challenge/entities/client_response/client_response.dart';
import 'package:tots_challenge/features/clients/bloc/clients_bloc.dart';
import 'package:tots_challenge/repositories/client_repo.dart';

void main() {
  group('Clients bloc', () {
    late ClientsBloc clientsBloc;
    late ClientsState clientsState;

    late ClientRepository clientRepo;

    setUp(() {
      clientRepo = MockClientRepository();
      clientsBloc = ClientsBloc(clientRepo: clientRepo);
      clientsState = const ClientsState();
    });

    group('Get clients', () {
      final clientResponse = MockClientResponse();

      blocTest(
        'Get clients event - success',
        build: () => clientsBloc,
        seed: () => clientsState,
        act: (bloc) => bloc.add(GetClients()),
        setUp: () {
          when(
            () => clientRepo.getClients(
              page: any(named: 'page'),
            ),
          ).thenAnswer((_) async => (clientResponse, null));
        },
        expect: () => [
          clientsState.copyWith(status: ClientsStatus.loading),
          clientsState.copyWith(
            status: ClientsStatus.success,
            clientResponse: clientResponse,
            clients: clientResponse.data ?? [],
            filteredClients: clientResponse.data ?? [],
          ),
        ],
      );

      blocTest(
        'Get clients event - failure',
        build: () => clientsBloc,
        seed: () => clientsState,
        act: (bloc) => bloc.add(GetClients()),
        setUp: () {
          when(
            () => clientRepo.getClients(
              page: any(named: 'page'),
            ),
          ).thenAnswer((_) async => (clientResponse, 'error'));
        },
        expect: () => [
          clientsState.copyWith(status: ClientsStatus.loading),
          clientsState.copyWith(
            status: ClientsStatus.failure,
            error: 'error',
          ),
        ],
      );

      blocTest(
        'Get clients event - failure on Exception',
        build: () => clientsBloc,
        seed: () => clientsState,
        act: (bloc) => bloc.add(GetClients()),
        setUp: () {
          when(
            () => clientRepo.getClients(
              page: any(named: 'page'),
            ),
          ).thenThrow(Exception());
        },
        expect: () => [
          clientsState.copyWith(status: ClientsStatus.loading),
          clientsState.copyWith(
            status: ClientsStatus.failure,
            error: 'Exception',
          ),
        ],
      );
    });

    group('Upsert client', () {
      final client = MockClient();

      setUp(() => registerFallbackValue(const Client()));

      blocTest(
        'Upsert client event - success',
        build: () => clientsBloc,
        seed: () => clientsState,
        act: (bloc) => bloc.add(UpsertClient(client: client)),
        setUp: () {
          when(
            () => clientRepo.upsertClient(
              client: any<Client>(named: 'client'),
            ),
          ).thenAnswer((_) async => (client, null));
        },
        expect: () => [
          clientsState.copyWith(status: ClientsStatus.loading),
          clientsState.copyWith(
            status: ClientsStatus.success,
          ),
        ],
      );

      blocTest(
        'Upsert client event - failure',
        build: () => clientsBloc,
        seed: () => clientsState,
        act: (bloc) => bloc.add(UpsertClient(client: client)),
        setUp: () {
          when(
            () => clientRepo.upsertClient(
              client: any(named: 'client'),
            ),
          ).thenAnswer((_) async => (client, 'error'));
        },
        expect: () => [
          clientsState.copyWith(status: ClientsStatus.loading),
          clientsState.copyWith(
            status: ClientsStatus.failure,
            error: 'error',
          ),
        ],
      );

      blocTest(
        'Upsert client event - failure on Exception',
        build: () => clientsBloc,
        seed: () => clientsState,
        act: (bloc) => bloc.add(UpsertClient(client: client)),
        setUp: () {
          when(
            () => clientRepo.upsertClient(
              client: any(named: 'client'),
            ),
          ).thenThrow(Exception());
        },
        expect: () => [
          clientsState.copyWith(status: ClientsStatus.loading),
          clientsState.copyWith(
            status: ClientsStatus.failure,
            error: 'Invalid client',
          ),
        ],
      );
    });

    group('Filter clients', () {
      blocTest(
        'Filter clients event - Nothing expected',
        build: () => clientsBloc,
        seed: () => clientsState,
        act: (bloc) => bloc.add(const FilterClients('query')),
      );
    });
  });
}

class MockClientRepository extends Mock implements ClientRepository {}

class MockClientResponse extends Mock implements ClientResponse {}

class MockClient extends Mock implements Client {}
