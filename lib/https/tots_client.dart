import 'package:dio/dio.dart' as dio;
import 'package:http/http.dart' as http;

/// Is a wrapper for the http package to make it easier to use.
class TotsClient {
  const TotsClient({required String baseUrl}) : _baseUrl = baseUrl;

  /// The base url for the api.
  final String _baseUrl;

  /// The dio instance.
  static dio.Dio dioInstance = dio.Dio(
    dio.BaseOptions(
      headers: {
        'Content-Type': 'application/json',
      },
    ),
  );

  /// Makes a get request to the api.
  Future<dio.Response<Map<String, dynamic>>> get(String path) async {
    dioInstance.options.baseUrl = _baseUrl;
    return dioInstance.get(path);
  }

  /// Makes a post request to the api.
  Future<dio.Response<Map<String, dynamic>>> post(
    String path, {
    Map<String, dynamic>? body,
  }) async {
    dioInstance.options.baseUrl = _baseUrl;
    final url = _baseUrl + path;
    return dioInstance.post(url, data: body);
  }

  /// Makes a post request to the api. Uses the http package because dio package
  /// does not working wiht certain API endpoints.
  Future<http.Response> postHttp(
    String path, {
    Map<String, dynamic>? body,
  }) async {
    final url = _baseUrl + path;
    return http.post(
      Uri.parse(url),
      body: body,
    );
  }

  /// Makes a put request to the api.
  Future<dio.Response<Map<String, dynamic>>> put(
    String path,
    Map<String, String> body,
  ) async {
    dioInstance.options.baseUrl = _baseUrl;
    return dioInstance.put(_baseUrl + path, data: body);
  }

  /// Makes a delete request to the api.
  Future<dio.Response<Map<String, dynamic>>> delete(String path) async {
    dioInstance.options.baseUrl = _baseUrl;
    return dioInstance.delete(_baseUrl + path);
  }
}
