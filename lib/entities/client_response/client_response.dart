// ignore: depend_on_referenced_packages
// ignore_for_file: non_constant_identifier_names

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:tots_challenge/entities/client/client.dart';

part 'client_response.freezed.dart';
part 'client_response.g.dart';

@freezed

/// The response from the server when getting clients.
class ClientResponse with _$ClientResponse {
  const factory ClientResponse({
    List<Client>? data,
    String? first_page_url,
    String? last_page_url,
    List<Link>? links,
    String? next_page_url,
    String? path,
    int? per_page,
    String? prev_page_url,
    int? to,
    int? total,
    int? last_page,
    int? from,
    int? current_page,
  }) = _ClientResponse;

  factory ClientResponse.fromJson(Map<String, dynamic> json) =>
      _$ClientResponseFromJson(json);
}

@freezed
class Link with _$Link {
  const factory Link({
    String? url,
    String? label,
    bool? active,
  }) = _Link;

  factory Link.fromJson(Map<String, dynamic> json) => _$LinkFromJson(json);
}
