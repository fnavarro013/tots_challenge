// ignore_for_file: non_constant_identifier_names
// ignore: depend_on_referenced_packages
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed

/// The user entity.
class User with _$User {
  factory User({
    int? id,
    String? firstname,
    String? lastname,
    String? email,
    String? photo,
    String? phone,
    dynamic facebookId,
    int? role,
    DateTime? created_at,
    DateTime? updated_at,
    int? status,
    int? isNotification,
    dynamic caption,
    String? tokenType,
    String? access_token,
  }) = _User;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}
