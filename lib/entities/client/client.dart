// ignore: depend_on_referenced_packages
import 'package:freezed_annotation/freezed_annotation.dart';

part 'client.freezed.dart';
part 'client.g.dart';

@freezed

/// A class that represents a client
class Client with _$Client {
  const factory Client({
    int? id,
    String? firstname,
    String? lastname,
    String? email,
    String? address,
    String? photo,
    String? caption,
    DateTime? createdAt,
    DateTime? updatedAt,
    int? deleted,
  }) = _Client;

  const Client._();

  factory Client.fromJson(Map<String, dynamic> json) => _$ClientFromJson(json);
}
