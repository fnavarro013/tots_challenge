import 'dart:convert';
import 'dart:developer';

import 'package:tots_challenge/entities/client/client.dart';
import 'package:tots_challenge/entities/client_response/client_response.dart';
import 'package:tots_challenge/repositories/base.dart';

/// Repository for [Client].
class ClientRepository extends RepositoryBase {
  /// Get all [Client]s.
  Future<(ClientResponse, String?)> getClients({required int page}) async {
    final response = await totsClient.postHttp(
      '/client/list?page=$page',
    );
    if (response.statusCode != 200) {
      throw Exception('Failed to get  clients');
    }

    final body = jsonDecode(response.body) as Map? ?? {};

    final bodyResponse = ClientResponse.fromJson(
      body['response'] as Map<String, dynamic>? ?? {},
    );

    final error = body['error'] as Map? ?? {};

    final errorMessage = error['message'] as String?;

    log('The current page is:${bodyResponse.current_page}');
    return (bodyResponse, errorMessage);
  }

  /// Create or update a [Client].
  Future<(Client, String?)> upsertClient({
    required Client client,
  }) async {
    final response = await totsClient.post(
      '/client/save',
      body: client.toJson(),
    );
    if (response.statusCode != 200) {
      throw Exception('Failed to create client');
    }

    final body = response.data?['response'] as Map<String, dynamic>? ?? {};

    final error = response.data?['error'] as Map<String, dynamic>? ?? {};

    final errorMsg = error['message'] as String?;

    return (Client.fromJson(body), errorMsg);
  }
}
