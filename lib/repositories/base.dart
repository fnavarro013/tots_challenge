import 'package:tots_challenge/https/tots_client.dart';

/// All repositories have to inherit from this.
abstract class RepositoryBase {
  static late TotsClient globalClient;

  /// Get the [TotsClient] for this repository.
  TotsClient get totsClient => globalClient;
}
