import 'package:tots_challenge/entities/user/user.dart';
import 'package:tots_challenge/repositories/base.dart';

/// Repository for current User.
class UserRepository extends RepositoryBase {
  /// Login with email and password.
  Future<(User, String?)> login({
    required String email,
    required String password,
  }) async {
    final response = await totsClient.post(
      '/mia-auth/login',
      body: {
        'email': email,
        'password': password,
      },
    );
    if (response.statusCode != 200) {
      throw Exception('Failed to login');
    }

    final userMap = response.data?['response'] as Map<String, dynamic>? ?? {};

    final error = response.data?['error'] as Map<String, dynamic>? ?? {};

    final errorMessage = error['message'] as String?;

    return (User.fromJson(userMap), errorMessage);
  }
}
