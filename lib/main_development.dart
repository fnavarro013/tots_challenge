import 'package:tots_challenge/app/app.dart';
import 'package:tots_challenge/bootstrap.dart';

void main() {
  bootstrap(
    () => const App(
      url: 'http://localhost:8080',
    ),
  );
}
