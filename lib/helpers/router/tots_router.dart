import 'package:auto_route/auto_route.dart';
import 'package:tots_challenge/helpers/router/tots_router.gr.dart';

/// A class that handles the navigation of the app.
@AutoRouterConfig()
class TotsRouter extends $TotsRouter {
  @override
  RouteType get defaultRouteType => const RouteType.material();

  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: LoginRoute.page, initial: true),
        AutoRoute(page: ClientsRoute.page),
      ];
}
