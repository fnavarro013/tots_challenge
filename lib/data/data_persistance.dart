import 'package:shared_preferences/shared_preferences.dart';

class DataPersistance {
  /// Obtain shared preferences.
  static Future<SharedPreferences> instance = SharedPreferences.getInstance();

  /// Save user token.
  static Future<void> saveToken(String token) async {
    final prefs = await DataPersistance.instance;
    await prefs.setString('token', token);
  }

  /// Get user token.
  static Future<String?> getToken() async {
    final prefs = await DataPersistance.instance;
    return prefs.getString('token');
  }
}
