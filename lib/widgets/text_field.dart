import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TotsTextField extends StatelessWidget {
  const TotsTextField({
    required this.controller,
    required this.hintText,
    this.obscureText = false,
    this.validator,
    this.onChanged,
    this.keyboardType,
    this.suffixIcon,
    this.inputFormatters,
    super.key,
  });

  /// The text to display inside the text field.
  final String hintText;

  /// Whether the text field should obscure the text being edited
  /// (e.g., for passwords).
  /// Defaults to false.
  final bool obscureText;

  /// Controls the text being edited.
  final TextEditingController controller;

  /// An optional method to pass to the text field to validate the input.
  final String? Function(String?)? validator;

  /// Called when the text being edited changes.
  final void Function(String)? onChanged;

  /// The type of information for which to optimize the text input.
  /// Defaults to [TextInputType.text].
  final TextInputType? keyboardType;

  /// An optional icon to display at the end of the text field.
  final Widget? suffixIcon;

  /// A list of input formatters to pass to the text field.
  final List<TextInputFormatter>? inputFormatters;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 295,
      child: TextFormField(
        controller: controller,
        obscureText: obscureText,
        validator: validator,
        onChanged: onChanged,
        keyboardType: keyboardType,
        inputFormatters: inputFormatters,
        decoration: InputDecoration(
          hintText: hintText,
          suffixIcon: suffixIcon,
          suffixIconColor: Theme.of(context).colorScheme.onPrimary,
        ),
      ),
    );
  }
}
