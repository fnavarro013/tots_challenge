import 'package:flutter/material.dart';

/// A widget that used to search for something.
class TotsSearchBar extends StatelessWidget {
  const TotsSearchBar({
    required this.hintText,
    required this.onChanged,
    super.key,
  });

  /// The text to display inside the search bar.
  final String hintText;

  /// Called when the text being edited changes.
  final void Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 36,
      child: SearchBar(
        hintText: hintText,
        onChanged: onChanged,
        padding: MaterialStateProperty.all<EdgeInsets>(
          const EdgeInsets.symmetric(horizontal: 20),
        ),
        leading: Icon(
          Icons.search,
          color: Theme.of(context).colorScheme.surface,
        ),
        shadowColor: MaterialStateProperty.all<Color>(
          Colors.transparent,
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(68),
            side: BorderSide(
              color: Theme.of(context).colorScheme.surface,
            ),
          ),
        ),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
        surfaceTintColor: MaterialStateProperty.all<Color>(Colors.white),
      ),
    );
  }
}
