import 'dart:ui';

import 'package:animated_background/animated_background.dart';
import 'package:flutter/material.dart';

/// A widget that generate particles in the background
/// of the widget tree.
class TotsParticles extends StatefulWidget {
  const TotsParticles({
    required this.child,
    super.key,
  });

  ///The widget below this widget in the tree.
  final Widget child;

  @override
  State<TotsParticles> createState() => _TotsParticlesState();
}

class _TotsParticlesState extends State<TotsParticles>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AnimatedBackground(
          vsync: this,
          behaviour: RandomParticleBehaviour(
            options: const ParticleOptions(
              baseColor: Color(0xFFe4f354),
              spawnMinSpeed: 15,
              spawnMaxSpeed: 28,
              spawnMinRadius: 70,
              spawnMaxRadius: 200,
              particleCount: 5,
              maxOpacity: 0.7,
              spawnOpacity: 0.2,
            ),
          ),
          child: const SizedBox.expand(),
        ),
        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 20, sigmaY: 20),
          child: const SizedBox.expand(),
        ),
        widget.child,
      ],
    );
  }
}
