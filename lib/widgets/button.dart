import 'package:flutter/material.dart';

/// Simple button widget that can be used throughout the app.
class TotsButton extends StatelessWidget {
  const TotsButton({
    required this.onPressed,
    required this.title,
    this.enabled = true,
    this.borderRadius,
    this.width,
    this.height,
    super.key,
  });

  /// Called when the button is tapped or otherwise activated.
  final VoidCallback onPressed;

  /// The text to display inside the button.
  final String title;

  /// The radius of the button's corners.
  /// Defaults to 34.
  final double? borderRadius;

  /// The width of the button.
  /// Defaults to 200.
  final double? width;

  /// The height of the button. If null, the height will be determined
  /// by the button's content.
  final double? height;

  /// Whether the button is enabled or not.
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: enabled ? onPressed : null,
      style: ButtonStyle(
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius ?? 34),
          ),
        ),
        minimumSize: MaterialStateProperty.all<Size>(
          Size(width ?? 200, height ?? 50),
        ),
      ),
      child: Text(
        title,
        style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: enabled
              ? Theme.of(context).colorScheme.onSecondary
              : Theme.of(context).colorScheme.onSecondary.withOpacity(0.2),
        ),
      ),
    );
  }
}
