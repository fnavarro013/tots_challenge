part of 'login_bloc.dart';

sealed class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

/// An event that is emitted when the user is trying to log in.
class LoginSubmitted extends LoginEvent {
  const LoginSubmitted({
    required this.email,
    required this.password,
  });

  /// The email of the user.
  final String email;

  /// The password of the user.
  final String password;

  @override
  List<Object> get props => [
        email,
        password,
      ];
}
