part of 'login_bloc.dart';

enum LoginStatus {
  /// The initial state of the login process.
  initial,

  /// The state of the login process when the user is being logged in.
  loading,

  /// The state of the login process when the user has been logged in.
  success,

  /// The state of the login process when the user has failed to log in.
  failure;

  bool get isInitial => this == LoginStatus.initial;
  bool get isLoading => this == LoginStatus.loading;
  bool get isSuccess => this == LoginStatus.success;
  bool get isFailure => this == LoginStatus.failure;
}

class LoginState extends Equatable {
  const LoginState({
    this.status = LoginStatus.initial,
    this.error = '',
    this.user,
  });

  /// The status of the login process.
  final LoginStatus status;

  /// The current user.
  final User? user;

  /// The error message to display if the login process failed.
  final String error;

  /// Returns a copy of this state with the given fields replaced with the new
  /// values.
  LoginState copyWith({
    LoginStatus? status,
    String? error,
    User? user,
  }) {
    return LoginState(
      status: status ?? this.status,
      error: error ?? this.error,
      user: user ?? this.user,
    );
  }

  @override
  List<Object?> get props => [
        status,
        error,
        user,
      ];
}
