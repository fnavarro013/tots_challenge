import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tots_challenge/data/data_persistance.dart';
import 'package:tots_challenge/entities/user/user.dart';
import 'package:tots_challenge/repositories/user_repo.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({
    required UserRepository userRepo,
  })  : _userRepo = userRepo,
        super(const LoginState()) {
    on<LoginSubmitted>(_onLoginSubmitted);
  }

  /// The user repository.
  final UserRepository _userRepo;

  /// Handles the [LoginSubmitted] event.
  FutureOr<void> _onLoginSubmitted(
    LoginSubmitted event,
    Emitter<LoginState> emit,
  ) async {
    emit(state.copyWith(status: LoginStatus.loading));
    try {
      /// Login with email and password.
      /// The response contains the user and the error message.
      final response = await _userRepo.login(
        email: event.email,
        password: event.password,
      );

      // If the error message is not null, then the login failed.
      if (response.$2 != null) {
        emit(
          state.copyWith(
            status: LoginStatus.failure,
            error: response.$2,
          ),
        );
        return;
      } else {
        final token = response.$1.access_token;

        /// Save the token to the local storage.
        await DataPersistance.saveToken(token!);

        emit(
          state.copyWith(
            status: LoginStatus.success,
            user: response.$1,
          ),
        );
      }
    } catch (e) {
      emit(
        state.copyWith(
          status: LoginStatus.failure,
          error: e.toString(),
        ),
      );
    }
  }
}
