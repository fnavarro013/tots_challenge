import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tots_challenge/features/login/bloc/login_bloc.dart';
import 'package:tots_challenge/helpers/router/tots_router.gr.dart';
import 'package:tots_challenge/l10n/l10n.dart';
import 'package:tots_challenge/repositories/user_repo.dart';
import 'package:tots_challenge/widgets/widget.dart';

///A widget that represents the Login page of the app
@RoutePage()
class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginBloc(
        userRepo: context.read<UserRepository>(),
      ),
      child: const LoginView(),
    );
  }
}

///A widget that represents the Login view of the [LoginPage]
class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> with TickerProviderStateMixin {
  /// The controller for the email text field.
  final _emailController = TextEditingController();

  /// The controller for the password text field.
  final _passwordController = TextEditingController();

  /// The key for the form.
  final _formKey = GlobalKey<FormState>();

  /// Whether the password is visible or not.
  bool _isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: _listener,
      child: Scaffold(
        body: TotsParticles(
          child: SizedBox.expand(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Hero(
                    tag: 'logo',
                    child: Image.asset(
                      'assets/logos/logo.png',
                      width: 282,
                    ),
                  ),
                  Text(
                    context.l10n.login.toUpperCase(),
                    style: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  TotsTextField(
                    controller: _emailController,
                    keyboardType: TextInputType.emailAddress,
                    validator: _emailValidator,
                    hintText: context.l10n.email,
                  ),
                  const SizedBox(height: 23),
                  TotsTextField(
                    controller: _passwordController,
                    keyboardType: TextInputType.visiblePassword,
                    hintText: context.l10n.password,
                    obscureText: !_isPasswordVisible,
                    validator: _passwordValidator,
                    suffixIcon: IconButton(
                      onPressed: () => setState(
                        () => _isPasswordVisible = !_isPasswordVisible,
                      ),
                      icon: Icon(
                        _isPasswordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                  ),
                  const SizedBox(height: 52),
                  TotsButton(
                    width: 296,
                    onPressed: onPressed,
                    title: context.l10n.loginButton.toUpperCase(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// The listener for the [LoginBloc].
  void _listener(BuildContext context, LoginState state) {
    /// If the login was successful, navigate to the home page.
    if (state.status.isSuccess) {
      Navigator.of(context).pop();
      context.router.push(const ClientsRoute());
    }

    /// If the login failed, show a snackbar with the error message.
    if (state.status.isFailure) {
      Navigator.of(context).pop();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(state.error),
          backgroundColor: Theme.of(context).colorScheme.error,
        ),
      );
    }

    /// If the login process is loading, show a loading indicator.
    if (state.status.isLoading) {
      showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (context) => Center(
          child: CircularProgressIndicator(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
      );
    }
  }

  /// Called when the login button is pressed.
  void onPressed() {
    /// Validate the form.
    // if (_formKey.currentState!.validate()) {
    /// Submit the login request.
    context.read<LoginBloc>().add(
          LoginSubmitted(
            email: _emailController.text,
            password: _passwordController.text,
          ),
        );
    // }
  }

  /// Validates the email.
  String? _emailValidator(String? value) {
    /// Validate the email.
    if (value == null || value.isEmpty) {
      return context.l10n.pleaseEnterEmail;
    } else if (!value.contains('@')) {
      return context.l10n.invalidEmail;
    }
    return null;
  }

  /// Validates the password.
  String? _passwordValidator(String? value) {
    /// Validate the password.
    if (value == null || value.isEmpty) {
      return context.l10n.pleaseEnterPassword;
    } else if (value.length < 6) {
      return context.l10n.passwordMustBeAtLeast6Characters;
    }
    return null;
  }
}
