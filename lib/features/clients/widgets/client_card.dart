import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tots_challenge/entities/client/client.dart';
import 'package:tots_challenge/l10n/l10n.dart';

/// A card that displays a client's information.
class ClientCard extends StatelessWidget {
  const ClientCard({
    required this.client,
    required this.onEditPressed,
    super.key,
  });

  /// The client to display.
  final Client client;

  /// Called when the edit button is pressed.
  final VoidCallback onEditPressed;

  @override
  Widget build(BuildContext context) {
    /// The client's full name.
    final name = '${client.firstname} ${client.lastname}';

    final hasImg = client.photo != null;

    final isUrl =
        hasImg && (client.photo!.startsWith('http') || client.photo!.isEmpty);

    /// The client's image.
    final clientImg =
        (hasImg && !isUrl) ? base64Decode(client.photo ?? '') : null;

    if (isUrl) {
      return ClientCardView.fromUrl(
        client: client,
        onEditPressed: onEditPressed,
        name: name,
        clientImg: client.photo!,
      );
    } else {
      return ClientCardView.fromUint8List(
        client: client,
        onEditPressed: onEditPressed,
        name: name,
        clientImg: clientImg!,
      );
    }
  }
}

class ClientCardView extends StatelessWidget {
  const ClientCardView({
    required this.child,
    required this.client,
    required this.onEditPressed,
    required this.name,
    super.key,
  });

  /// Creates a [ClientCardView] from a [Uint8List].
  factory ClientCardView.fromUint8List({
    required Client client,
    required VoidCallback onEditPressed,
    required String name,
    required Uint8List clientImg,
  }) {
    return ClientCardView(
      client: client,
      onEditPressed: onEditPressed,
      name: name,
      child: CircleAvatar(
        radius: 25,
        backgroundColor: Colors.transparent,
        backgroundImage: MemoryImage(clientImg),
      ),
    );
  }

  /// Creates a [ClientCardView] from a URL.
  /// If the URL is empty, a default icon will be displayed.
  factory ClientCardView.fromUrl({
    required Client client,
    required VoidCallback onEditPressed,
    required String name,
    required String clientImg,
  }) {
    return ClientCardView(
      client: client,
      onEditPressed: onEditPressed,
      name: name,
      child: CircleAvatar(
        radius: 25,
        backgroundColor: Colors.transparent,
        backgroundImage: clientImg.isNotEmpty ? NetworkImage(clientImg) : null,
        child: clientImg.isEmpty
            ? const Icon(
                Icons.person,
                color: Colors.white,
              )
            : null,
      ),
    );
  }

  /// The client to display.
  final Client client;

  /// Called when the edit button is pressed.
  final VoidCallback onEditPressed;

  /// The client's full name.
  final String name;

  /// The child to display in the client's avatar.
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Theme.of(context).colorScheme.onSecondary,
        border: Border.all(
          color: Theme.of(context).colorScheme.surface,
          width: 1.2,
        ),
      ),
      child: Row(
        children: [
          const SizedBox(width: 20),
          CircleAvatar(
            radius: 25,
            backgroundColor: Theme.of(context).colorScheme.surface,
            child: child,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                ),
                Text(
                  client.email ?? '',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Theme.of(context).colorScheme.surface,
                  ),
                ),
              ],
            ),
          ),
          const Spacer(),
          PopupMenuButton(
            color: Theme.of(context).colorScheme.primary,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            itemBuilder: (context) {
              return [
                PopupMenuItem<Widget>(
                  onTap: onEditPressed,
                  child: Row(
                    children: [
                      Icon(
                        Icons.edit,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                      const SizedBox(width: 8),
                      Text(
                        context.l10n.edit,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Theme.of(context).colorScheme.onSecondary,
                        ),
                      ),
                    ],
                  ),
                ),
              ];
            },
          ),
        ],
      ),
    );
  }
}
