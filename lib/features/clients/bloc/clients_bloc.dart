import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tots_challenge/entities/client/client.dart';
import 'package:tots_challenge/entities/client_response/client_response.dart';
import 'package:tots_challenge/repositories/client_repo.dart';

part 'clients_event.dart';
part 'clients_state.dart';

class ClientsBloc extends Bloc<ClientsEvent, ClientsState> {
  ClientsBloc({
    required ClientRepository clientRepo,
  })  : _clientRepo = clientRepo,
        super(const ClientsState()) {
    on<GetClients>(_getClients);
    on<UpsertClient>(_upserClient);
    on<FilterClients>(_filterClients);
  }

  final ClientRepository _clientRepo;

  /// Handles the [GetClients] event.
  FutureOr<void> _getClients(
    GetClients event,
    Emitter<ClientsState> emit,
  ) async {
    if (state.status.isLoading) return;
    emit(state.copyWith(status: ClientsStatus.loading));

    try {
      /// The current page of the clients.
      final currentPage = state.clientResponse?.current_page ?? 0;

      /// The next page of the clients.
      final nextPage = currentPage + 1;

      /// The last page of the clients.
      final lastPage = state.clientResponse?.last_page ?? 1;

      // If the current page is the last page, then the clients have already
      // been loaded.
      if (currentPage == lastPage) {
        emit(state.copyWith(status: ClientsStatus.success));
        return;
      }

      /// Get the clients from the server. The response contains the clients and
      /// the error message.
      ///
      /// The endpoint is paginated, so we need to pass the page number.
      /// Currently, the page size is 50.
      final response = await _clientRepo.getClients(page: nextPage);

      if (response.$2 != null) {
        emit(
          state.copyWith(
            status: ClientsStatus.failure,
            error: response.$2,
          ),
        );
        return;
      }
      emit(
        state.copyWith(
          status: ClientsStatus.success,
          clients: [...state.clients, ...response.$1.data ?? []],
          filteredClients: [
            ...state.filteredClients,
            ...response.$1.data ?? [],
          ],
          clientResponse: response.$1,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(
          status: ClientsStatus.failure,
          error: e.toString(),
        ),
      );
    }
  }

  /// Handles the [UpsertClient] event.
  FutureOr<void> _upserClient(
    UpsertClient event,
    Emitter<ClientsState> emit,
  ) async {
    try {
      if (state.status.isLoading) return;
      emit(state.copyWith(status: ClientsStatus.loading));

      final response = await _clientRepo.upsertClient(
        client: event.client,
      );

      final upserClient = response.$1;

      final clients = List<Client>.from(state.clients);

      final filteredClients = List<Client>.from(state.filteredClients);

      // If the client already exists, then update it in the list.
      final clientIdx = clients.indexWhere((c) => c.id == upserClient.id);

      if (clientIdx != -1) {
        clients[clientIdx] = upserClient;
      }

      // If the client already exists, then update it in the filtered list.
      final filteredIdx =
          filteredClients.indexWhere((c) => c.id == upserClient.id);

      if (filteredIdx != -1) {
        filteredClients[filteredIdx] = upserClient;
      }

      // If the client doesn't exist, then add it to the list.
      if (response.$2 != null) {
        emit(
          state.copyWith(
            status: ClientsStatus.failure,
            error: response.$2,
          ),
        );
        return;
      }

      emit(
        state.copyWith(
          status: ClientsStatus.success,
          clients: clients,
          filteredClients: filteredClients,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(
          status: ClientsStatus.failure,
          error: 'Invalid client',
        ),
      );
    }
  }

  /// Handles the [FilterClients] event.
  FutureOr<void> _filterClients(
    FilterClients event,
    Emitter<ClientsState> emit,
  ) async {
    emit(state.copyWith(status: ClientsStatus.initial));
    final query = event.query.toLowerCase();

    /// Filter clients name by query. If the query matches the first name or the
    /// last name, then the client is added to the filtered list.
    final filteredClients = state.clients.where(
      (e) {
        final firstName = RegExp(".*${query.split('').join('.*')}.*")
            .hasMatch((e.firstname ?? '').toLowerCase());

        final lastName = RegExp(".*${query.split('').join('.*')}.*")
            .hasMatch((e.lastname ?? '').toLowerCase());

        return firstName || lastName;
      },
    ).toList();

    /// If the query is empty, then the filtered list is the same as the clients
    /// list.
    if (query.isEmpty) {
      emit(state.copyWith(filteredClients: state.clients));
    } else {
      emit(state.copyWith(filteredClients: filteredClients));
    }
  }
}
