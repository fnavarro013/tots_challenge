part of 'clients_bloc.dart';

enum ClientsStatus {
  /// The initial state of the clients.
  initial,

  /// The state of the clients when the clients are being loaded.
  loading,

  /// The state of the clients when the clients are being created or updated.
  updating,

  /// The state of the clients when the clients have been loaded.
  success,

  /// The state of the clients when the clients have failed to load.
  failure;

  bool get isInitial => this == ClientsStatus.initial;
  bool get isLoading => this == ClientsStatus.loading;
  bool get isUpdating => this == ClientsStatus.updating;
  bool get isSuccess => this == ClientsStatus.success;
  bool get isFailure => this == ClientsStatus.failure;
}

class ClientsState extends Equatable {
  const ClientsState({
    this.status = ClientsStatus.initial,
    this.error = '',
    this.clients = const [],
    this.filteredClients = const [],
    this.clientResponse,
  });

  /// The status of the clients.
  final ClientsStatus status;

  /// The error message to display if the clients something failed.
  final String error;

  /// The list of clients.
  final List<Client> clients;

  /// The list of clients filtered by the search query.
  final List<Client> filteredClients;

  /// The response from the server when getting clients.
  final ClientResponse? clientResponse;

  /// Whether the current page is the last page.
  bool get isLastPage {
    if (clientResponse == null) return false;
    return clientResponse?.last_page == clientResponse?.current_page;
  }

  /// Returns a copy of this state with the given fields replaced with the new
  /// values.
  ClientsState copyWith({
    ClientsStatus? status,
    String? error,
    List<Client>? clients,
    List<Client>? filteredClients,
    ClientResponse? clientResponse,
  }) {
    return ClientsState(
      status: status ?? this.status,
      error: error ?? this.error,
      clients: clients ?? this.clients,
      filteredClients: filteredClients ?? this.filteredClients,
      clientResponse: clientResponse ?? this.clientResponse,
    );
  }

  @override
  List<Object?> get props => [
        status,
        error,
        clients,
        filteredClients,
        clientResponse,
      ];
}
