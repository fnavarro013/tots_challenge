part of 'clients_bloc.dart';

sealed class ClientsEvent extends Equatable {
  const ClientsEvent();

  @override
  List<Object> get props => [];
}

class ClientsLoaded extends ClientsEvent {}

/// An event that is emitted when the user is trying to get clients.
class GetClients extends ClientsEvent {}

/// An event that is emitted when the user is trying to create or edit a client.
class UpsertClient extends ClientsEvent {
  const UpsertClient({
    required this.client,
  });

  /// The client to create.
  final Client client;

  @override
  List<Object> get props => [
        client,
      ];
}

/// An event that is emitted when the user is filtering clients.
class FilterClients extends ClientsEvent {
  const FilterClients(this.query);

  /// The query to filter clients by.
  final String query;

  @override
  List<Object> get props => [
        query,
      ];
}
