import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tots_challenge/entities/client/client.dart';
import 'package:tots_challenge/features/clients/bloc/clients_bloc.dart';
import 'package:tots_challenge/l10n/l10n.dart';
import 'package:tots_challenge/widgets/widget.dart';

/// A modal that allows the user to create or edit a client.
class CreateEditClientModal extends StatefulWidget {
  const CreateEditClientModal({
    this.client,
    super.key,
  });

  /// The client to edit. If null, a new client will be created.
  final Client? client;

  @override
  State<CreateEditClientModal> createState() => _CreateEditClientModalState();

  /// Shows the [CreateEditClientModal] modal.
  Future<void> show(BuildContext context) async {
    await showDialog<void>(
      context: context,
      builder: (_) => BlocProvider.value(
        value: context.read<ClientsBloc>(),
        child: CreateEditClientModal(
          client: client,
        ),
      ),
    );
  }
}

class _CreateEditClientModalState extends State<CreateEditClientModal> {
  /// The controller for the first name text field.
  final _firstNameController = TextEditingController();

  /// The controller for the last name text field.
  final _lastNameController = TextEditingController();

  /// The controller for the email text field.
  final _emailController = TextEditingController();

  /// The key for the form.
  final _formKey = GlobalKey<FormState>();

  /// Whether the client is being edited or not.
  bool isEdit = false;

  /// The image to display. If null, an empty image will be displayed.
  XFile? _image;

  @override
  void initState() {
    if (widget.client != null) {
      final isUrl = widget.client!.photo!.isNotEmpty &&
          widget.client!.photo!.startsWith('http');

      _firstNameController.text = widget.client!.firstname ?? '';
      _lastNameController.text = widget.client!.lastname ?? '';
      _emailController.text = widget.client!.email ?? '';
      if (isUrl) {
        _image = XFile(widget.client!.photo!);
      } else {
        _image = XFile.fromData(
          base64Decode(widget.client!.photo!),
          path: 'temp',
        );
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    /// Instance of [AppLocalizations].
    final l10n = context.l10n;

    /// Whether the client has an image or not.
    final hasImage = widget.client?.photo?.isNotEmpty ?? false;

    /// Whether to show the image or not.
    final showImage = hasImage && (_image?.path.isNotEmpty ?? false);

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Padding(
        padding: const EdgeInsets.all(25).copyWith(bottom: 15),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.client != null ? l10n.editClient : l10n.addNewClient,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(height: 48),
                Align(
                  child: DottedBorder(
                    borderType: BorderType.Circle,
                    dashPattern: const [8, 4],
                    color: Theme.of(context).colorScheme.onBackground,
                    child: GestureDetector(
                      onTap: _pickImage,
                      child: CircleAvatar(
                        radius: 60,
                        backgroundColor:
                            Theme.of(context).colorScheme.onSecondary,
                        child: showImage
                            ? _ImageWidget(
                                image: _image,
                                imgUrl: widget.client?.photo,
                                isEdit: isEdit,
                              )
                            : const _EmptyImageWidget(),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 19),
                TotsTextField(
                  controller: _firstNameController,
                  hintText: '${l10n.firstName}*',
                  validator: _validator,
                  inputFormatters: [LengthLimitingTextInputFormatter(15)],
                ),
                const SizedBox(height: 13),
                TotsTextField(
                  controller: _lastNameController,
                  hintText: '${l10n.lastName}*',
                  validator: _validator,
                  inputFormatters: [LengthLimitingTextInputFormatter(15)],
                ),
                const SizedBox(height: 13),
                TotsTextField(
                  controller: _emailController,
                  hintText: '${l10n.emailAddress}*',
                  keyboardType: TextInputType.emailAddress,
                  validator: _emailValidator,
                ),
                const SizedBox(height: 48),
                Row(
                  children: [
                    Expanded(
                      child: TextButton(
                        onPressed: Navigator.of(context).pop,
                        child: Text(l10n.cancel.toUpperCase()),
                      ),
                    ),
                    const SizedBox(width: 18),
                    Expanded(
                      child: TotsButton(
                        onPressed: _onPressed,
                        title: l10n.save.toUpperCase(),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Picks an image from the gallery. If the image is valid, it will be
  /// displayed. It will also set the [isEdit] flag to true and the [_image]
  /// will be compressed to 200x200.
  void _pickImage() {
    ImagePicker()
        .pickImage(source: ImageSource.gallery, maxHeight: 200, maxWidth: 200)
        .then((value) {
      if (value != null) {
        setState(() {
          isEdit = true;
          _image = value;
        });
      }
    });
  }

  /// Validates the input. Returns null if the input is valid.
  String? _validator(String? value) {
    if (value == null || value.isEmpty) {
      return context.l10n.requiredField;
    }
    return null;
  }

  /// Validates the email address. Returns null if the email address is valid.
  String? _emailValidator(String? value) {
    if (value == null || value.isEmpty) {
      return context.l10n.requiredField;
    }
    if (!value.contains('@')) {
      return context.l10n.invalidEmail;
    }
    return null;
  }

  /// Called when the save button is pressed.
  /// If the form is valid and an image is selected, the client will be saved.
  ///
  /// On call to this method, the client will be created or edited
  /// depending on the client value.
  void _onPressed() {
    if (_formKey.currentState!.validate() && _image != null) {
      String? encondeImg;
      if (isEdit) {
        final fileImg = File(_image!.path);
        encondeImg = base64Encode(fileImg.readAsBytesSync());
      }

      final client = Client(
        id: widget.client?.id,
        firstname: _firstNameController.text,
        lastname: _lastNameController.text,
        email: _emailController.text,
        photo: encondeImg ?? widget.client?.photo ?? '',
        createdAt: widget.client?.createdAt ?? DateTime.now(),
        updatedAt: DateTime.now(),
      );

      context.read<ClientsBloc>().add(UpsertClient(client: client));

      Navigator.of(context).pop();
    } else {
      return;
    }
  }
}

/// A widget that displays an image when given an [XFile].
class _ImageWidget extends StatelessWidget {
  const _ImageWidget({
    required this.image,
    required this.isEdit,
    this.imgUrl,
  });

  /// The image to display.
  final XFile? image;

  /// If the image is a URL, the URL to the image.
  final String? imgUrl;

  /// Whether the image is being edited or not.
  final bool isEdit;

  @override
  Widget build(BuildContext context) {
    if (image != null && isEdit) {
      return ClipOval(
        child: Image.file(
          File(image!.path),
          width: 120,
          height: 120,
          fit: BoxFit.cover,
        ),
      );
    }

    if (imgUrl != null && imgUrl!.startsWith('http')) {
      return ClipOval(
        child: Image.network(
          imgUrl!,
          width: 120,
          height: 120,
          fit: BoxFit.cover,
        ),
      );
    } else {
      return ClipOval(
        child: Image.memory(
          base64Decode(imgUrl!),
          width: 120,
          height: 120,
          fit: BoxFit.cover,
        ),
      );
    }
  }
}

/// A widget that displays an empty image.
class _EmptyImageWidget extends StatelessWidget {
  const _EmptyImageWidget();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'assets/icons/upload_image.png',
          width: 30,
        ),
        const SizedBox(height: 10),
        Text(
          context.l10n.uploadImage,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
      ],
    );
  }
}
