import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tots_challenge/features/clients/bloc/clients_bloc.dart';
import 'package:tots_challenge/features/clients/modals/modals.dart';
import 'package:tots_challenge/features/clients/widgets/client_widgets.dart';
import 'package:tots_challenge/l10n/l10n.dart';
import 'package:tots_challenge/widgets/widget.dart';

///A widget that represents the Clients page of the app
@RoutePage()
class ClientsPage extends StatelessWidget {
  const ClientsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ClientsBloc(
        clientRepo: RepositoryProvider.of(context),
      )..add(GetClients()),
      child: const ClientsView(),
    );
  }
}

///A widget that represents the Clients view of the [ClientsPage]
class ClientsView extends StatelessWidget {
  const ClientsView({super.key});

  @override
  Widget build(BuildContext context) {
    /// Instance of the [AppLocalizations] class
    final l10n = context.l10n;

    /// The list of clients to display
    final clients = context.watch<ClientsBloc>().state.filteredClients;

    return BlocListener<ClientsBloc, ClientsState>(
      listener: _listener,
      child: Scaffold(
        body: Stack(
          children: [
            TotsParticles(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 34),
                    Hero(
                      tag: 'logo',
                      child: Align(
                        child: Image.asset(
                          'assets/logos/logo.png',
                          width: 128,
                        ),
                      ),
                    ),
                    Text(
                      l10n.clients.toUpperCase(),
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: Theme.of(context).colorScheme.surface,
                      ),
                    ),
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Expanded(
                          child: TotsSearchBar(
                            hintText: '${l10n.search}...',
                            onChanged: (value) => context
                                .read<ClientsBloc>()
                                .add(FilterClients(value)),
                          ),
                        ),
                        const SizedBox(width: 18),
                        TotsButton(
                          width: 93,
                          height: 29,
                          onPressed: () =>
                              const CreateEditClientModal().show(context),
                          title: l10n.addNew.toUpperCase(),
                        ),
                      ],
                    ),
                    const SizedBox(height: 18),
                    Expanded(
                      child: ListView.separated(
                        physics: const BouncingScrollPhysics(),
                        itemCount: clients.length,
                        separatorBuilder: (_, __) => const SizedBox(height: 16),
                        itemBuilder: (context, index) {
                          return ClientCard(
                            client: clients[index],
                            onEditPressed: () => CreateEditClientModal(
                              client: clients[index],
                            ).show(context),
                          );
                        },
                      ),
                    ),
                    const SizedBox(height: 100),
                  ],
                ),
              ),
            ),
            Positioned.fill(
              bottom: 36,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: TotsButton(
                  width: 296,
                  enabled: !context.watch<ClientsBloc>().state.isLastPage,
                  title: l10n.loadMore.toUpperCase(),
                  onPressed: () =>
                      context.read<ClientsBloc>().add(GetClients()),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// The listener for the [ClientsBloc].
  void _listener(
    BuildContext context,
    ClientsState state,
  ) {
    if (state.status.isFailure) {
      Navigator.of(context).pop();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(state.error),
          backgroundColor: Theme.of(context).colorScheme.error,
        ),
      );
    }

    if (state.status.isLoading) {
      showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (context) => Center(
          child: CircularProgressIndicator(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
      );
    }

    if (state.status.isSuccess) {
      Navigator.of(context).pop();
    }

    if (state.isLastPage) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(context.l10n.noMoreClientsToLoad),
          backgroundColor: Theme.of(context).colorScheme.tertiary,
        ),
      );
    }
  }
}
