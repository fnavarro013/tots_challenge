import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tots_challenge/helpers/router/tots_router.dart';
import 'package:tots_challenge/https/tots_client.dart';
import 'package:tots_challenge/l10n/l10n.dart';
import 'package:tots_challenge/repositories/base.dart';
import 'package:tots_challenge/repositories/client_repo.dart';
import 'package:tots_challenge/repositories/user_repo.dart';
import 'package:tots_challenge/themes/theme.dart';

class App extends StatefulWidget {
  const App({
    required this.url,
    super.key,
  });

  /// The url of the Tots server.
  final String url;

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  // Initialize the router
  late TotsRouter _router;

  @override
  void initState() {
    RepositoryBase.globalClient = TotsClient(
      baseUrl: widget.url,
    );
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _router = TotsRouter();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(create: (_) => ClientRepository()),
        RepositoryProvider(create: (_) => UserRepository()),
      ],
      child: MaterialApp.router(
        theme: TotsTheme.theme,
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        routerConfig: _router.config(),
      ),
    );
  }
}
