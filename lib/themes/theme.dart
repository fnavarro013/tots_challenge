import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

/// The custom theme for the app.
class TotsTheme {
  static final ThemeData theme = ThemeData(
    brightness: Brightness.light,
    dividerColor: Colors.white54,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor:
            MaterialStateProperty.all<Color>(const Color(0xFF0D1111)),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(34),
          ),
        ),
      ),
    ),
    colorScheme: const ColorScheme(
      primary: Color(0xFF0D1111),
      secondary: Color(0xFF0D1111),
      surface: Color(0xFF434545),
      background: Colors.white,
      error: Colors.red,
      onPrimary: Color(0xFFa3aab1),
      onSurface: Color(0xFF0D1111),
      onSecondary: Colors.white,
      onBackground: Color(0xFF0D1111),
      onError: Color(0xFF0D1111),
      tertiary: Colors.green,
      onTertiary: Color(0xFFE4F353),
      brightness: Brightness.light,
    ),
  );
}
